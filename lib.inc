section .text

; Принимает код возврата и завершает текущий процесс
exit:
	mov rax, 60
	syscall

; Принимает указатель на нуль-терминированную строку, возвращает её длину
string_length:
	xor rax, rax
.count:
	cmp byte [rdi+rax], 0
	je .end
	inc rax
	jmp .count
.end:
	ret

; Принимает указатель на нуль-терминированную строку, выводит её в stdout
print_string:
        push rdi
	call string_length
        pop rdi
	mov rdx, rax
	mov rax, 1
	mov rsi, rdi
	mov rdi, 1
	syscall
	ret


print_newline:
	mov rdi, 0xA
; Принимает код символа и выводит его в stdout
print_char:
        push rdi
        mov rsi, rsp
        mov rdi, 1
        mov rdx, 1
        mov rax, 1
        syscall
        
        pop rdi
        ret



; Выводит беззнаковое 8-байтовое число в десятичном формате
; Совет: выделите место в стеке и храните там результаты деления
; Не забудьте перевести цифры в их ASCII коды.
print_uint:
        push rax
        push rdi
        push 0x0
        mov rax, rdi
        mov rdi, 10
.to_str:
        xor rdx, rdx
        div rdi
        add rdx, '0'
        push rdx
        cmp rax, 0
        je .print_intstr
        jmp .to_str
.print_intstr:
        pop rdi
        cmp rdi, 0x0
        je .end
        call print_char
        jmp .print_intstr
.end:

        pop rdi
        pop rax
        ret

; Выводит знаковое 8-байтовое число в десятичном формате

print_int:
        mov rax, rdi
        cmp rax, 0
        jge .print_unsigned
.print_signed:
	push rdi
        mov rdi, '-'
        call print_char
	pop rdi
        neg rdi
.print_unsigned:
        jmp print_uint

; Принимает два указателя на нуль-терминированные строки, возвращает 1 если они равны, 0 иначе
string_equals:
        push rbx
        push rdx

        xor rax, rax
        xor rcx, rcx
        xor rdx, rdx
        xor rbx, rbx
.loop:
        mov  bl, [rdi + rcx]
        mov  dl, [rsi + rcx]
        cmp bl, 0x0
        je .res
        cmp dl, 0x0
        je .res
        cmp bl, dl
        jne .res
        inc rcx
        jmp .loop
.res:
        xor rcx, rcx
        cmp bl, dl
        je .yes
        pop rdx
        pop rbx
        xor rax, rax
        ret
.yes:
        pop rdx
        pop rbx
        mov rax, 1
        ret


; Читает один символ из stdin и возвращает его. Возвращает 0 если достигнут конец потока
read_char:
	push  0x0
	mov rsi, rsp
	xor rdi, rdi
	mov rdx, 1
	xor rax, rax
	syscall
	pop rax
	ret



; Принимает: адрес начала буфера, размер буфера
; Читает в буфер слово из stdin, пропуская пробельные символы в начале, .
; Пробельные символы это пробел 0x20, табуляция 0x9 и перевод строки 0xA.
; Останавливается и возвращает 0 если слово слишком большое для буфера
; При успехе возвращает адрес буфера в rax, длину слова в rdx.
; При неудаче возвращает 0 в rax
; Эта функция должна дописывать к слову нуль-терминатор

read_word:
	xor rcx, rcx

.spaces:
	push rdi
	push rsi
	push rdx
	push rcx
	call read_char
	pop rcx
	pop rdx
	pop rsi
	pop rdi

	cmp rax, 0x20
	je .spaces
	cmp rax, 0x9
	je .spaces
	cmp rax, 0xA
	je .spaces
	cmp rax, 0
	je .letters_end
.letters:
	cmp rax, 0
	je .letters_end
	cmp rax, 0x20
	je .letters_end
	cmp rax, 0x9
	je .letters_end
	cmp rax, 0xA
	je .letters_end

	cmp rcx, rsi
	jge .err
	mov byte[rdi + rcx], al
	inc rcx

	push rdi
	push rsi
	push rdx
	push rcx
	call read_char
	pop rcx
	pop rdx
	pop rsi
	pop rdi
	jmp .letters

.letters_end:
	mov byte[rdi+rcx], 0
	mov rdx, rcx
	mov rax, rdi
	jmp .exit

.err:
	xor rax, rax
.exit:
	ret


; Принимает указатель на строку, пытается
; прочитать из её начала беззнаковое число.
; Возвращает в rax: число, rdx : его длину в символах
; rdx = 0 если число прочитать не удалось
parse_uint:
        xor r8, r8
        xor rcx, rcx
        mov r10, 10
        xor rax, rax

.first_symbol:
        mov r8b, [rdi + rcx]
        inc rcx
        cmp r8b, '0'
        jb .err
        cmp r8b, '9'
        ja .err
        cmp r8b, 0
        je .err
        sub r8b, '0'
        mov al, r8b

.next:
        mov r8b, [rdi + rcx]
        cmp r8b, '0'
        jb .ret_num
        cmp r8b, '9'
        ja .ret_num
        cmp r8b, 0
        je .ret_num
        inc rcx
        mul r10
        sub r8b, '0'
        add rax, r8
        jmp .next
.err:
        xor rdx, rdx
        jmp .exit

.ret_num:
        mov rdx, rcx

.exit:
        ret




; Принимает указатель на строку, пытается
; прочитать из её начала знаковое число.
; Если есть знак, пробелы между ним и числом не разрешены.
; Возвращает в rax: число, rdx : его длину в символах (включая знак, если он был)
; rdx = 0 если число прочитать не удалось
parse_int:
        xor r8, r8
        xor rcx, rcx
        mov r10, 10
        xor rax, rax

.first_symbol:
        mov r8b, [rdi + rcx]
        cmp r8b, '-'
        je .read_signed
        call parse_uint
        ret

.read_signed:
        inc rdi
        call parse_uint
        cmp rdx, 0
        je .exit
        inc rdx
        neg rax

.exit:
        ret




; Принимает указатель на строку, указатель на буфер и длину буфера
; Копирует строку в буфер
; Возвращает длину строки если она умещается в буфер, иначе 0
string_copy:
        xor rax, rax
	push rdi
	call string_length
	pop rdi
	inc rax
	cmp rax, rdx
	jg .err
.read:
        xchg rsi, rdi
	mov rcx, rax
	cld
	rep movsb
	jmp .exit
.err:
        xor rax, rax
        jmp .exit
.exit:
        ret
